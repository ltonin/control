function [crossed, index, correct] = ctrl_crossing_threshold(y, thresholds, correctidx)

    % Check if y is in interval [0 1]
    if(min(y) < 0 || max(y) > 1)
        error('chk:input', 'y must be in the interval [0 1]');
    end

    % Check that two thresholds are provided
    if(length(thresholds) ~= 2)
        error('chk:input', 'two thresholds should be provided');
    end
    
    % Check if the threshold are in the ascending order
    if(diff(thresholds) < 0)
        error('chk:input', 'thresholds should be provided in ascending order');
    end

    % If provided, check the correctidx
    if (nargin == 3)
        if( length(correctidx) ~= 1)
            error('chk:input', 'only one correct index should be provided');
        end
    
        if( ismember(correctidx, [1 2]) == false)
            error('chk:input', 'correct index should be 1 or 2');
        end
    end
   
    crossed = true;
    index = find(y <= thresholds(1) | y >= thresholds(2), 1, 'first');
    
    if isempty(index)
        index   = nan;
        crossed = false;
    end
    
    correct = nan;
    if nargin == 3 && crossed == true
        switch(correctidx)
            case 1
                correct = y(index) <= thresholds(1);
            case 2
                correct = y(index) >= thresholds(2);
        end
    end
    
    if (nargout == 3) && (nargin ~= 3)
        warning('chk:output', 'To determine the correctness, the correct threshold id must be provided (nan is reported now).');
    end
end