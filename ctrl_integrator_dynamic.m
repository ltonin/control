function y = ctrl_integrator_dynamic(x, yp, support)
% y = ctrl_integrator_dynamic(x, yp, support)
%
% CTRL_INTEGRATOR_DYNAMIC returns the integrated signal given x the raw BCI
% output and yp the previous state.
%
% Input:
%   - x         Value(s) of the current bci output. The values must be in 
%               the interval (0, 1)
%   - yp        Previous state (previous output of the function or initial
%               state of the system)
%   - support   Support structure with the following fields:
%               - support.forcefree  SEE ctrl_integrator_dynamic_forcefree
%                                    for the inner fields
%               - support.forcebci   SEE ctrl_integrator_dynamic_forcebci
%                                    for the inner fields
%               - support.dt         The temporal interval for the
%                                    integration. [Default: 0.0625 ms]
%               - support.phi        Parameter to balance the contribution
%                                    of ffree and fbci. The parameter must
%                                    be in the interval [0 1]
%               - support.chi        Parameter for the general strength of
%                                    the integration
% Output:
%   - y         Integrated output according to the dynamic integration. The
%               output is limited between [0 1].
%
% SEE ALSO: ctrl_integrator_dynamic, ctrl_integrator_dynamic_forcebci,
% ctrl_integrator_dynamic_forcefree


    if( (x < 0.0) || (x > 1.0) )
        error('chk:x', 'the provided x must be in the interval [0 1]');
    end
    
    if( (yp < 0.0) || (yp > 1.0) )
        error('chk:yp', 'the provided yp must be in the interval [0 1]');
    end
    
    if(isfield(support, 'dt') == false)
        support.dt = 1/16;
    end
    
    if(isfield(support, 'phi') == false)
        error('chk:sup', 'support structure must have field ''phi''');
    end
    
    if(isfield(support, 'chi') == false)
        error('chk:sup', 'support structure must have field ''chi''');
    end
    
    if( (support.phi < 0.0) || (support.phi > 1.0) )
        error('chk:phi', 'the provided phi must be in the interval [0 1]');
    end

    % Normalization factor (using as input the whole interval (0,1)
    ynorm = 0:0.01:1;
    fnorm = max(abs(ctrl_integrator_dynamic_forcefree(ynorm, support.forcefree)));

    % Force free
    ffree = support.phi*ctrl_integrator_dynamic_forcefree(yp, support.forcefree)./fnorm;
    
    % Force bci
    fbci = (1-support.phi)*ctrl_integrator_dynamic_forcebci(x, support.forcebci);
    
    % Integrated output
    y = yp + support.dt*support.chi*(ffree + fbci);
    
    % Limiting the integrated output between 0 and 1
    if (y > 1)
        y = 1;
    elseif (y < 0)
        y = 0;
    end
end