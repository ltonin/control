function ybci = ctrl_integrator_dynamic_forcebci(x, support)
% ybci = ctrl_integrator_dynamic_forcebci(x, support)
%
% CTRL_INTEGRATOR_DYNAMIC_FORCEBCI returns the force of the bci given the
% current bci output x.
%
% Input:
%   - x         Value(s) of the current bci output. The values must be in 
%               the interval (0, 1)
%   - support   Support structure with the following fields:
%               - support.coefficient   The coefficients for the polyval
%               function to generate the function. If the field is not
%               provided the default coefficients are used: 
%               [-0.0000    6.4000    0.0000    0.4000    0.0000]
%
% SEE ALSO: ctrl_integrator_dynamic, ctrl_integrator_dynamic_forcebci
    
    if( (min(x) < 0.0) || (max(x) > 1.0) )
        error('chk:x', 'the provided x must be in the interval [0 1]');
    end

    if(isfield(support, 'coefficient') == false)
        support.coefficient = [-0.0000    6.4000    0.0000    0.4000    0.0000];
    end
   
    coefficient = support.coefficient;
    
    ybci = polyval(coefficient, x-0.5);

end