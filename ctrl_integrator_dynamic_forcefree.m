function yfree = ctrl_integrator_dynamic_forcefree(y, support)
% yfree = ctrl_integrator_dynamic_forcefree(y, support)
%
% CTRL_INTEGRATOR_DYNAMIC_FORCEFREE returns the force of system given the
% current state y.
%
% Input:
%   - y         Value(s) of the current state. The values must be in the
%               interval (0, 1)
%   - support   Support structure with the following required fields:
%               - support.psi   Height of the valley 
%               - support.omega Width of the valley (in ]0 0.5[
%
% SEE ALSO: ctrl_integrator_dynamic, ctrl_integrator_dynamic_forcebci


    if( (min(y) < 0.0) || (max(y) > 1.0) )
        error('chk:y', 'the provided x must be in the interval [0 1]');
    end

    if(isfield(support, 'psi') == false)
        error('chk:sup', 'support structure must have field ''psi''');
    end
    
    if(isfield(support, 'omega') == false)
        error('chk:sup', 'support structure must have field ''omega''');
    end
    
    if(length(support.psi) ~= 1)
        error('chk:sup', 'psi parameter must be a scalar');
    end
    
    if(length(support.omega) ~= 1)
        error('chk:sup', 'omega parameter must be a scalar');
    end
    
    if( (support.omega < 0.0) || (support.omega > 0.5) )
        error('chk:sup', 'omega parameter must be in ]0.0 0.5[');
    end
    

    psi   = support.psi;
    omega = support.omega;

    yfree = zeros(size(y));
    for idx = 1:length(y)
        cy = y(idx);
        if( cy >= 0 && cy < (0.5-omega) )
            yfree(idx) = -sin( (pi/(0.5-omega)).*cy);
        elseif(cy > (0.5 + omega) && cy <= 1)
            yfree(idx) = sin( (pi/(0.5-omega)).*(cy-0.5-omega));
        elseif(cy >= (0.5 - omega) && cy <= (0.5 + omega))
            yfree(idx) = -psi.*sin(pi.*(cy - 0.5)./omega);
        end
    end


end