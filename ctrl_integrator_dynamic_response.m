function y = ctrl_integrator_dynamic_response(x, support)
% y = ctrl_integrator_dynamic_response(x, support)
%
% CTRL_INTEGRATOR_DYNAMIC_RESPONSE applies the dynamic integrator framework
% to a vector of input raw probabilities x.
%
% Input:
%   - x         Vector of input raw probabilities [samples x 1]. Values
%               must be between 0 and 1.
%   - support:  Support structure for the integrator framework. SEE
%               ctrl_integrator_dynamic for more details
% Output:
%   - y         Vector of integrated probabilities
%
% SEE ALSO: ctrl_integrator_dynamic, ctrl_integrator_dynamic_forcebci,
% ctrl_integrator_dynamic_forcefree

    if isfield(support, 'forcebci') == false
        support.forcebci = [];
    end    

    NumSamples = length(x);
    y = 0.5*ones(NumSamples, 1);
    
    for sId = 2:NumSamples
        prev_value = y(sId-1);
        curr_prob  = x(sId);
        y(sId) = ctrl_integrator_dynamic(curr_prob, prev_value, support);
    end

end