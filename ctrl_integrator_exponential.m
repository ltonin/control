function y = ctrl_integrator_exponential(x, yp, support)
% y = ctrl_integrator_exponential(x, yp, support)
%
% CTRL_INTEGRATOR_EXPONENTIAL applies an exponential smoothing integrator 
% to the provided input x, given the previous state yp. In addition it 
% checks if the current value of x has to be rejected.
%
% Input:
%   - x         Scalar value to be integrated. It must be in (0, 1)
%               interval
%   - yp        Previous state (previous output of the function or initial
%               state of the system)
%   - support   Support structure with the following required fields:
%               - support.alpha     => alpha integrator parameter
%                                      [scalar in (0,1)]
%               - support.rejection => rejection parameter. If a scalar 
%                                      number is provided then it refers 
%                                      to the higher value (e.g., 0.55). 
%                                      Two scalars can be provided one
%                                      per each class (e.g., [0.45 0.55])

    if( (x < 0.0) || (x > 1.0) )
        error('chk:x', 'the provided x must be in the interval [0 1]');
    end

    if(isfield(support, 'alpha') == false)
        error('chk:sup', 'support structure must have field ''alpha''');
    end
    
    if(isfield(support, 'rejection') == false)
        error('chk:sup', 'support structure must have field ''rejection''');
    end
    
    
    if(length(support.rejection) == 1)
        support.rejection = [(1-support.rejection) support.rejection];
    end
    
    if(length(support.rejection) == 2 && diff(support.rejection) < 0)
         error('chk:sup', 'If two values are provided for the rejection, the they have to be sorted in ascendent order');
    end
    
    if(length(support.rejection) > 2)
         error('chk:sup', 'Maximum two rejection values (or 1 for both classes) can be provided');
    end
   
    
    alpha     = support.alpha;
    rejection = support.rejection;
    

    % If 1-rejection < x < rejection then do not update the integrated
    % probability ( => using x=yp)
    if(x >= rejection(1) && x <= rejection(2))
        x = yp;
    end
    
    % Exponential smoothing formula
    y = alpha*yp + (1-alpha)*x;

end